package com.example.hexa3.launcher

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("com.example.hexa3") // TODO: understand why this is needed - why prev annotation doesn't find all I need
class LauncherApplication

fun main(args: Array<String>) {
    runApplication<LauncherApplication>(*args)
}
