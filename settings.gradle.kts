rootProject.name = "hexa3"

include(
    "core-business-logic",
    "core-input",
    "core-model",
    "core-output",
    "input-web-adapter",
    "launcher",
    "output-h2-adapter",
)
