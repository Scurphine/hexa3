package com.example.hexa3.coremodel.model

import java.time.LocalDateTime

data class EnrichedConsignment(
    val id: Long,
    val reference: ConsignmentReference,
    val createDate: LocalDateTime,
)
