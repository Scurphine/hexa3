package com.example.hexa3.coremodel.model

data class ValidConsignment(
    val reference: ConsignmentReference
)

data class ConsignmentReference(private val reference: String) {
    init {
        if (reference.length != 17 || reference.trim().length != 17)
            throw IllegalArgumentException("Invalid reference '$reference")
    }

    val value
        get() = reference
}
