plugins {
    id("org.springframework.boot")
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation("org.springframework:spring-context")

    testImplementation("org.junit.jupiter:junit-jupiter")
}
