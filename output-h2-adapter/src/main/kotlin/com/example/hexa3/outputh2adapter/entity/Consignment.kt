package com.example.hexa3.outputh2adapter.entity

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "consignment")
open class Consignment(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    open val id: Long = 0,

    @Column(name = "reference", length = 17, nullable = false)
    open var reference: String,

    @Column(name = "create_date", nullable = false)
    open val createDate: LocalDateTime,
)
