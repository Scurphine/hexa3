package com.example.hexa3.outputh2adapter.service

import com.example.hexa3.coremodel.model.ConsignmentReference
import com.example.hexa3.coremodel.model.EnrichedConsignment
import com.example.hexa3.coremodel.model.ValidConsignment
import com.example.hexa3.coreoutput.port.ConsignmentOutputPort
import com.example.hexa3.outputh2adapter.entity.Consignment
import com.example.hexa3.outputh2adapter.repository.ConsignmentRepository
import java.time.LocalDateTime
import org.springframework.stereotype.Service

@Service
class ConsignmentPersistenceService(
    private val consignmentRepository: ConsignmentRepository
) : ConsignmentOutputPort {
    override fun createConsignment(input: ValidConsignment): EnrichedConsignment = input
        .run { Consignment(reference = reference.value, createDate = LocalDateTime.now()) }
        .run(consignmentRepository::save)
        .run { EnrichedConsignment(id, ConsignmentReference(reference), createDate) }
}
