package com.example.hexa3.outputh2adapter.repository

import com.example.hexa3.outputh2adapter.entity.Consignment
import org.springframework.data.jpa.repository.JpaRepository

interface ConsignmentRepository : JpaRepository<Consignment, Long>
