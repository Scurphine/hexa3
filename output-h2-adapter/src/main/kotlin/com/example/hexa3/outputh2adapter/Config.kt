package com.example.hexa3.outputh2adapter

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@EnableJpaRepositories
@ComponentScan("com.example.hexa3.outputh2adapter")
@EntityScan("com.example.hexa3.outputh2adapter.entity")
class Config
