package com.example.hexa3.corebusinesslogic.usecase

import com.example.hexa3.coremodel.model.EnrichedConsignment
import com.example.hexa3.coremodel.model.ValidConsignment
import com.example.hexa3.coreoutput.port.ConsignmentOutputPort
import org.springframework.stereotype.Component

@Component
class SaveValidConsignmentUseCase(
    private val consignmentOutputPort: ConsignmentOutputPort
): BaseUseCase<ValidConsignment, EnrichedConsignment> {
    override operator fun invoke(input: ValidConsignment): EnrichedConsignment = consignmentOutputPort
        .createConsignment(input)
}
