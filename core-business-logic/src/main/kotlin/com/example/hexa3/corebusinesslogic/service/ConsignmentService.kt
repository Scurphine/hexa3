package com.example.hexa3.corebusinesslogic.service

import com.example.hexa3.corebusinesslogic.usecase.SaveValidConsignmentUseCase
import com.example.hexa3.coreinput.port.ConsingmentInputPort
import com.example.hexa3.coremodel.model.EnrichedConsignment
import com.example.hexa3.coremodel.model.ValidConsignment
import org.springframework.stereotype.Service

@Service
class ConsignmentService(
    private val saveValidConsignment: SaveValidConsignmentUseCase,
) : ConsingmentInputPort {
    override fun createConsignment(input: ValidConsignment): EnrichedConsignment = input
        .run(saveValidConsignment::invoke)

}
