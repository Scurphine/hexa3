package com.example.hexa3.corebusinesslogic.usecase

interface BaseUseCase<in I, out O> {
    operator fun invoke(input: I): O
}
