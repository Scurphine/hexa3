package com.example.hexa3.coreoutput.port

import com.example.hexa3.coremodel.model.EnrichedConsignment
import com.example.hexa3.coremodel.model.ValidConsignment

interface ConsignmentOutputPort {
    fun createConsignment(input: ValidConsignment): EnrichedConsignment
}
