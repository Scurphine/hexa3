package com.example.hexa3.coreinput.port

import com.example.hexa3.coremodel.model.EnrichedConsignment
import com.example.hexa3.coremodel.model.ValidConsignment

interface ConsingmentInputPort {
    fun createConsignment(input: ValidConsignment): EnrichedConsignment
}
