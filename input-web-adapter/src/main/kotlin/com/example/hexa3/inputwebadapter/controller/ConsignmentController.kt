package com.example.hexa3.inputwebadapter.controller

import com.example.hexa3.inputwebadapter.dto.ConsignmentCreateRequest
import com.example.hexa3.inputwebadapter.dto.ConsignmentDTO
import com.example.hexa3.inputwebadapter.service.ConsignmentWebService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/consignment")
class ConsignmentController(
    private val consignmentWebService: ConsignmentWebService
) {
    @PostMapping
    fun createConsignment(@RequestBody requestBody: ConsignmentCreateRequest): ConsignmentDTO = consignmentWebService
        .createConsignment(requestBody)

//    fun getConsignmentCommon(@PathVariable("reference") reference: String) = consignmentWebService
//        .getConsignmentCommon(reference)
}
