package com.example.hexa3.inputwebadapter.service

import com.example.hexa3.coreinput.port.ConsingmentInputPort
import com.example.hexa3.coremodel.model.ConsignmentReference
import com.example.hexa3.coremodel.model.ValidConsignment
import com.example.hexa3.inputwebadapter.dto.ConsignmentCreateRequest
import com.example.hexa3.inputwebadapter.dto.ConsignmentDTO
import com.example.hexa3.inputwebadapter.dto.toDTO
import org.springframework.stereotype.Service

@Service
class ConsignmentWebService(
    private val consingmentInputPort: ConsingmentInputPort
) {
    fun createConsignment(requestBody: ConsignmentCreateRequest): ConsignmentDTO =
        ValidConsignment(ConsignmentReference(requestBody.reference))
            .run(consingmentInputPort::createConsignment)
            .toDTO()
}
