package com.example.hexa3.inputwebadapter.dto

import com.example.hexa3.coremodel.model.EnrichedConsignment
import java.time.LocalDateTime

data class ConsignmentDTO(val reference: String, val createDate: LocalDateTime)

fun EnrichedConsignment.toDTO() = ConsignmentDTO(reference.value, createDate)
