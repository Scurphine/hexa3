package com.example.hexa3.inputwebadapter.dto

data class ConsignmentCreateRequest(val reference: String)
